## Status
Be able to run the following OGL application:
* off-screen rendering
  * simple triangle: https://github.com/yuq/gfx/tree/master/gbm-surface
  * vertex shader uniform: https://github.com/yuq/gfx/tree/master/gbm-surface-move
  * multi varying: https://github.com/yuq/gfx/tree/master/gbm-surface-color
  * multi draw: https://github.com/yuq/gfx/tree/master/gbm-surface-draw
  * FBO: https://github.com/yuq/gfx/tree/master/gbm-surface-fbo
  * **kmscube**: https://github.com/yuq/kmscube
     * ./kmscube -d -D /dev/dri/renderD128
     * ./kmscube -d -D /dev/dri/renderD128 -M rgba
* on-screen rendering
  * PRIME export/import: https://github.com/enunes/mesa-test-programs/blob/master/gbm-bo-test.c
  * renderonly lib: https://github.com/enunes/mesa-test-programs/blob/master/eglkms.c
  * **kmscube**: https://cgit.freedesktop.org/mesa/kmscube/
     * ./kmscube
     * ./kmscube -M rgba
     * ./kmscube -A
  * **glmark2**: https://github.com/glmark2/glmark2
     * glmark2-drm -b build
     * glmark2-es2-drm -b build
  * **kodi-gbm**: https://github.com/xbmc/xbmc
* desktop
  * Xorg+modesetting+glamor
      * es2gears
  * weston
      * es2gears

## Tested on
* Mali400
  * Allwinner A10/A20/H3/A64
  * Rockchip RK3188
  * Exynos 4412
* Mali450
  * Allwinner H5
  * Rockchip RK3328
  * Amlogic S905X

## Development
Lima driver has been upstreamed to linux kernel and mesa, so we need to work on the upstream repo now. The mesa/linux repo in this lima gitlab group is deprecated. But we can still utilize this place for non-code needs.

### kernel driver
kernel driver changes need to be send to drm-misc repo here https://cgit.freedesktop.org/drm/drm-misc/. There is no gitlab project for it, so we have to send patches to dri-devel mailing list for review, please also CC lima mailing list to get lima developers' attention.

### mesa driver
mesa driver changes need to be send to mesa repo here https://gitlab.freedesktop.org/mesa/mesa. We prefer to send gitlab MR for review which ease the review process and has CI test. Please add `lima` MR tag to get lima developers' attention.

After review pass, please rebase the MR with reviewers' Reviewed-by/Acked-by/Tested-by tags added and select the checkbox of: `Allow commits from members who can merge to the target branch` to permit other developers merge your MR if you don't have the merge permission.

Reference: https://www.mesa3d.org/submittingpatches.html

### issues
For lima spec issues and open discussion, we may continue using this lima gitlab group.

### doc
For project topic, still this WIKI. For technical topic, we may write into upstream repos, like kernel Documentation/gpu/lima.rst.

## Build and install
### common process
1. build Linux kernel with lima driver from: https://cgit.freedesktop.org/drm/drm-misc/
2. build mesa with lima driver from: https://gitlab.freedesktop.org/mesa/mesa

mesa build
```
meson build -Dvulkan-drivers=[] -Dplatforms=drm,x11 -Ddri-drivers=[] -Dgallium-drivers=lima,kmsro
ninja -C build install
```

### buildroot
For those who want to use buildroot:
https://github.com/enunes/buildroot-external-lima

### OpenEmbedded
meson metadata with lima driver and test apps:
https://github.com/superna9999/meta-meson/tree/topic/mesa-lima

## Tool
* dump the runtime close source mali driver memory for reverse engineering
  * https://gitlab.freedesktop.org/lima/mali-syscall-tracker

## TODO
### mesa
* Newly refined GP compiler needs optimization, code size can be ~40% smaller compared to close source driver output, main problem should be creating too many moves but too few reg load/store
* PP compiler optimization, add post scheduler instruction combination which combine multi instructions into one instruction
* both GP and PP compiler need control flow and other un-implemented instruction support
* texture mipmap support
  * still miss multi level render target support for auto generate mipmap
* Build a piglit test result list so that we can start clear them one by one.
* X11 desktop support
  * https://gitlab.freedesktop.org/lima/mesa/issues/56#note_98690
* Wayland desktop support
  * https://gitlab.freedesktop.org/lima/mesa/issues/59
* optimization
  * thread rendering that has dedicated render thread to submit command

### kernel
* Power management

## Done
### mesa
* GP compiler needs re-fine so that we can guarantee the vertex shader compilation succeed, Connor Abbott think out a new algorithm which should work
* Mesa renderonly lib support
* multi varying support, current implementation assumes we only have a single varying (gl_Position), command stream setup need be implemented for multi varying support
* command stream setup for multi draw, flush at once
* Mali450 initial support
* PP uniform support, command stream need be setup for PP uniform
* fix render fail when buffer is 800x600, maybe due to buffer alignment or command stream setup
* break the ABI come from libdrm_lima which is integrated into mesa for more efficiency especially the command submit interface
* texture support, both PP compiler and command stream need be changed
* command stream setup refine for multi FBO and dynamic buffer size
* Mali450 fully support
* EGL_ANDROID_native_fence_sync support
* Tiled texture format support
* implement EGL_KHR_partial_update
* New mesa NIR changes will always lower io/alu to scalar which is not expected by PP, need merge scalar back to vector or create a PIPE_CAP to disable it, see
  * https://www.mail-archive.com/mesa-dev@lists.freedesktop.org/msg189216.html

### kernel
* PRIME export support
* Mali450 initial support
* New task scheduler which can create a per OGL context fence context. We can use the new drm_sched which is from amdgpu and will be adopted by etnaviv too. drm_sched should be in Linux kernel 4.16.
* New memory management for non-continuous buffer object
* Different GPU mCLK frequency setup, current GPU mCLK frequency is the default frequency when system boot, but GPU should be able to run on a higher frequency.
* Mali450 fully support, include multi group GP/PP, DLBU
* submit support syn fd dep
* use TTM as MM

## Reference
* Luc Verhaegen's original Lima site: http://web.archive.org/web/20180106112822/http://limadriver.org/
* compiler: https://gitorious.org/open-gpu-tools/cwabbotts-open-gpu-tools/ 
